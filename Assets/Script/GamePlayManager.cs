﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayManager : MonoBehaviour {

    #region Singleton
    public static GamePlayManager instance;
    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

    }
    #endregion

    public BreakerManager breakerManager;
    public BreakerPool breakerPool;
    public SpawnManager spawnManager;

    private bool isPendingChecking = false;
    private BreakerType currentBreakerType;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mouse = Input.mousePosition;
            mouse.z = 10;

            Vector3 ray = Camera.main.ScreenToWorldPoint(mouse);
            RaycastHit2D hit = Physics2D.Raycast(ray, Vector2.zero);

            if (hit && hit.collider != null)
            {
                if (hit.collider.gameObject.GetComponent<Breaker>())
                {
                    Debug.Log("Tapped some shit.");
                    currentBreakerType = hit.collider.gameObject.GetComponent<Breaker>().breakerType;
                    isPendingChecking = true;
                    hit.collider.gameObject.GetComponent<Breaker>().Tapped();
                }
            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            if(isPendingChecking)
            {
                breakerManager.checker.gameObject.SetActive(false);
                spawnManager.ApplyRemoval(currentBreakerType);
                isPendingChecking = false;
            }
        }
    }
}
