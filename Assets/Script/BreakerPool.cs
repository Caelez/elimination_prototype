﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakerPool : MonoBehaviour {

    [SerializeField] private BreakerObject[] breakerObject;
    private Dictionary<BreakerType, Queue<GameObject>> breakerQueue;

	// Use this for initialization
	void Awake () {
        Initiate();
	}

    public GameObject SpawnBreaker(BreakerType _type, Vector3 _position, Quaternion _rotation)
    {
        GameObject _go = null;
        if (breakerQueue[_type].Count > 0) _go = breakerQueue[_type].Dequeue();
        else _go = SpawnExtraBreaker(_type);
        _go.SetActive(true);
        _go.transform.position = _position;
        _go.transform.rotation = _rotation;
        return _go;
    }

    public GameObject SpawnBreaker(BreakerType _type, Transform _parent)
    {
        return SpawnBreaker(_type, _parent.position, _parent.rotation);
    }

    public void DespawnBreaker(GameObject _go)
    {
        breakerQueue[_go.GetComponent<Breaker>().breakerType].Enqueue(_go);
        _go.transform.position = gameObject.transform.position;
        _go.transform.rotation = gameObject.transform.rotation;
        _go.SetActive(false);
    }

    #region Private Method
    private void Initiate()
    {
        breakerQueue = new Dictionary<BreakerType, Queue<GameObject>>();
        foreach(BreakerObject _bo in breakerObject)
        {
            Queue<GameObject> tempQ = new Queue<GameObject>();
            for(int i = 0; i < _bo.size; i++)
            {
                GameObject _go = Instantiate(_bo.prefab, gameObject.transform);
                _go.SetActive(false);
                tempQ.Enqueue(_go);
            }
            breakerQueue.Add(_bo.type, tempQ);
        }
    }
    private GameObject SpawnExtraBreaker(BreakerType _type)
    {
        foreach(BreakerObject _bo in breakerObject)
        {
            if (_bo.type == _type)
            {
                return Instantiate(_bo.prefab, gameObject.transform);
            }
        }
        return null;
    }
    #endregion

    #region Subclass
    [System.Serializable]
    private class BreakerObject
    {
        public BreakerType type;
        public GameObject prefab;
        public int size;
    }
    #endregion
}
