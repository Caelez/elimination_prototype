﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckObject : MonoBehaviour {

    public BreakerType currentCheckType;
    public Queue<GameObject> pendingCheckList = new Queue<GameObject>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Breaker _b = collision.gameObject.GetComponent<Breaker>();
        if (_b.breakerType == currentCheckType && !_b.isChecked)
        {
            //Debug.Log("Something Came in: " + _b.breakerType);
            pendingCheckList.Enqueue(_b.gameObject);            
        }
        //Debug.Log("Pending:" + pendingCheckList.Count);
    }

}
