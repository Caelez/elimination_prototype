﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakerManager : MonoBehaviour {

    public CheckObject checker;
    public Material breakerShader;
    public Dictionary<BreakerType, List<GameObject>> sortingBreakerList = new Dictionary<BreakerType, List<GameObject>>();

    private Queue<GameObject> nextCheckingQueue = new Queue<GameObject>();

	// Use this for initialization
	void Start () {
        checker.gameObject.SetActive(false);
        sortingBreakerList.Add(BreakerType.A, new List<GameObject>());
        sortingBreakerList.Add(BreakerType.B, new List<GameObject>());
        sortingBreakerList.Add(BreakerType.C, new List<GameObject>());
        sortingBreakerList.Add(BreakerType.D, new List<GameObject>());
        sortingBreakerList.Add(BreakerType.E, new List<GameObject>());
    }

    public void ApplyChecking(GameObject _firstTap)
    {
        Breaker _b = _firstTap.GetComponent<Breaker>();
        //_b.isChecked = true;
        //RaycastChecking(_firstTap);
        checker.currentCheckType = _b.breakerType;
        checker.gameObject.SetActive(true);
        checker.pendingCheckList.Enqueue(_firstTap);
        StartCoroutine(CheckingRoutine());
    }

    private void RaycastChecking(GameObject _currentCheckingBreaker)
    {
        Breaker _fBreaker = _currentCheckingBreaker.GetComponent<Breaker>();
        foreach (GameObject _go in sortingBreakerList[_fBreaker.breakerType])
        {
            Breaker _checkBreaker = _go.GetComponent<Breaker>();
            if (!_currentCheckingBreaker && !_checkBreaker.isChecked)
            {
                Vector3 direction = _currentCheckingBreaker.transform.position - _go.transform.position;
                RaycastHit2D hit = Physics2D.Raycast(_currentCheckingBreaker.transform.position, direction);
                if(hit.transform.gameObject == _go)
                {
                    Debug.Log("Distance: " + hit.distance);
                    Debug.DrawLine(_currentCheckingBreaker.transform.position, _go.transform.position, Color.green);
                    _go.GetComponent<Breaker>().ChangeShader(breakerShader);
                    _checkBreaker.isChecked = true;
                    nextCheckingQueue.Enqueue(_go);
                    GamePlayManager.instance.spawnManager.pendingDespawnList.Add(_go);
                }
            }
        }
        if(nextCheckingQueue.Count > 0) RaycastChecking(nextCheckingQueue.Dequeue());
    }

    private IEnumerator CheckingRoutine()
    {
        while (checker.pendingCheckList.Count > 0)
        {
            GameObject _go = checker.pendingCheckList.Dequeue();
            GamePlayManager.instance.spawnManager.pendingDespawnList.Add(_go);
            _go.GetComponent<Breaker>().isChecked = true;
            _go.GetComponent<Breaker>().ChangeShader(breakerShader);
            checker.transform.position = _go.transform.position;
            checker.transform.rotation = _go.transform.rotation;
            Debug.Log(checker.transform.position);
            yield return new WaitForSeconds(0.02f);            
        }        
        StopCoroutine(CheckingRoutine());
    }
}

public enum BreakerType
{
    A,
    B,
    C,
    D,
    E
}
