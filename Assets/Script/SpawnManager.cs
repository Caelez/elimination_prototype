﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    [SerializeField] private Transform[] spawnPoint;
    [SerializeField] private int capacity;
    public List<GameObject> breaker = new List<GameObject>();
    public List<GameObject> pendingDespawnList = new List<GameObject>();
    public List<GameObject> buggedList = new List<GameObject>();

	// Use this for initialization
	void Start () {
        StartCoroutine(CheckCapacity());
	}

    public void CheckAndPrepareRemoval(GameObject _go)
    {
        if(breaker.Contains(_go))
        {
            GamePlayManager.instance.breakerManager.ApplyChecking(_go);
        }
    }
    public void ApplyRemoval(BreakerType _type)
    {
        Debug.Log("Removing: " + pendingDespawnList.Count + " component(s).");
        foreach (GameObject _despawn in pendingDespawnList)
        {
            if (_despawn.GetComponent<Breaker>().breakerType == _type)
            {
                breaker.Remove(_despawn);
                GamePlayManager.instance.breakerPool.DespawnBreaker(_despawn);
            }
            else buggedList.Add(_despawn);
        }
        pendingDespawnList.Clear();
        StartCoroutine(CheckCapacity());
    }

    public IEnumerator CheckCapacity()
    {
        while(breaker.Count < capacity)
        {
            yield return new WaitForSeconds(0.05f);
            SpawnBreakerFromPool();
        }
        StopCoroutine(CheckCapacity());
    }

    private void SpawnBreakerFromPool()
    {
        //Debug.Log("Random number: " + a + ", Random Breaker Type: " + (BreakerType)a);
        GameObject _go = GamePlayManager.instance.breakerPool.SpawnBreaker((BreakerType)Random.Range(0, 5), spawnPoint[Random.Range(0, spawnPoint.Length)]);
        _go.GetComponent<Breaker>().Spawn();
        breaker.Add(_go);
    }
}
