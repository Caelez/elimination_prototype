﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Breaker : MonoBehaviour {

    public BreakerType breakerType;
    public bool isChecked = false;

    private Material originalShade;

    // Use this for initialization
    void Awake () {
        originalShade = gameObject.GetComponent<SpriteRenderer>().material;
    }

    public void Spawn()
    {
        isChecked = false;
        gameObject.GetComponent<SpriteRenderer>().material = originalShade;
    }

    public void ChangeShader(Material _shader)
    {
        gameObject.GetComponent<SpriteRenderer>().material = _shader;
    }

    public void Tapped()
    {
        Debug.Log("tapped");
        GamePlayManager.instance.spawnManager.CheckAndPrepareRemoval(gameObject);
    }
}
